voc-perturb 
----
![](https://img.shields.io/crates/v/voc-perturb.svg)


#### Create new annotated image dataset via perturbation of existing ones

Annotation of image data is time-consuming, this crate is a lazy person's tool for generating more annotated data
with as little effort as possible. Basically, all you need to do is

```
voc-perturb input_dataset_path output_dataset_name
```

See `voc-perturb -h` for help. 
