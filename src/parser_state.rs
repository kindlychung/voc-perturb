#[derive(PartialEq, Debug)]
pub enum ParserState {
    Xmin,
    Ymin,
    Xmax,
    Ymax,
    NoInterest,
}


impl ParserState {
    pub fn new() -> ParserState {
        ParserState::NoInterest
    }
    pub fn no_interest(&self) -> bool {
        *self == ParserState::NoInterest
    }
    pub fn set_no_interest(&mut self) {
        *self = ParserState::NoInterest;
    }
    pub fn is_interesting(&self) -> bool {
        !self.no_interest()
    }
    pub fn mutate_by_tag_name(&mut self, tag: &[u8]) {
        match tag {
            b"xmin" => {
                *self = ParserState::Xmin;
            }
            b"ymin" => {
                *self = ParserState::Ymin;
            }
            b"xmax" => {
                *self = ParserState::Xmax;
            }
            b"ymax" => {
                *self = ParserState::Ymax;
            }
            _ => {
                *self = ParserState::NoInterest;
            },
        }
    }
}
