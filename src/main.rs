#[macro_use]
extern crate clap;
extern crate voc_perturb;
extern crate rand;

use std::fs::read_dir;
use clap::{App};
use std::path::PathBuf;
use voc_perturb::file_mapping::{FileMapping};



fn main() {
    let mut rng = rand::thread_rng();
    let yaml = load_yaml!("../cli.yml");
    let matches = App::from_yaml(yaml).get_matches();
    let delta_x_percent: f32 = matches.value_of("delta_x_percent").unwrap_or("10").parse().expect("Failed to parse delta_x");
    let delta_y_percent: f32 = matches.value_of("delta_y_percent").unwrap_or("10").parse().expect("Failed to parse delta_x");
    let output_folder = matches.value_of("OUTPUT").unwrap();
    let images_pathbuf = PathBuf::new().join(matches.value_of("INPUT").unwrap()).join("JPEGImages");
    let dir_content = read_dir(&images_pathbuf).unwrap();
    let mut counter = 0;
    for entry in dir_content {
        let entry = entry.unwrap();
        let p = entry.path();
        let fm = FileMapping::new(p, output_folder);
        fm.process(delta_x_percent, delta_y_percent, &mut rng);
        counter += 1;
        if counter % 50 == 0 {
            println!("\rINFO: {} images processed.", counter);
        }
    }
}